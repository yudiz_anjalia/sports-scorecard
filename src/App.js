import React from 'react';
import './App.css';
import Score from './components/Score';

function App() {
  return (
    <div className="App">
     <Score/>
    </div>
  );
}

export default App;
