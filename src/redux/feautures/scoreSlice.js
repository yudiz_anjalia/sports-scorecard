import {createSlice , createAsyncThunk} from "@reduxjs/toolkit";

export const getScore = createAsyncThunk("score/getScore" , async () => {
    return fetch("https://backend.sports.info/api/v1/nba/game/f34b1dfd-97fd-4942-9c14-05a05eeb5921/summary").then((res) => 
    res.json()
    );
});
console.log("data")
const scoreSlice = createSlice({
    name:"score",
    initialState : {
        score:[],
        loading : false
    },
    extraReducers : {
        [getScore.pending] : (state , action) => {
            state.loading = true;
        },
        [getScore.fulfilled]: (state,action) => {
            state.loading = false;
            state.score = action.payload
        },
        [getScore.rejected]: (state , action) =>{
            state.loading = false
        },
    },
});

export default scoreSlice.reducer;