import {configureStore} from "@reduxjs/toolkit";
import ScoreReducer from "./feautures/scoreSlice";

export default configureStore({
    reducer : {
        score: ScoreReducer,
    },
});                                                     